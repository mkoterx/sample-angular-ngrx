export class Movie {
  constructor(public name: string,
              public rating: number,
              public releaseYear: number) {
  }
}
