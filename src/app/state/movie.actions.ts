import {Action} from '@ngrx/store';
import {Movie} from '../model/movie.mode';


export enum ActionTypes {
  LoadMovies = '[Movies] Load movies',
  LoadMoviesSuccess = '[Movies] Movies Loaded Success',
  LoadMoviesError = '[Movies] Movies Loaded Error',
}

export class LoadMovies implements Action {
  readonly type = ActionTypes.LoadMovies;
}

export class LoadMoviesSuccess implements Action {
  readonly type = ActionTypes.LoadMoviesSuccess;

  constructor(public payload: { movies: Movie[] }) {
  }
}

export class LoadMoviesError implements Action {
  readonly type = ActionTypes.LoadMoviesError;
}

export type ActionsUnion = LoadMovies | LoadMoviesSuccess | LoadMoviesError;
