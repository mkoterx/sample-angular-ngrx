import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {Movie} from '../model/movie.mode';
import {ActionTypes} from './movie.actions';

@Injectable()
export class MovieEffects {

  @Effect()
  loadMovies$ = this.actions$
    .pipe(
      ofType(ActionTypes.LoadMovies),
      mergeMap(() => this.loadMovies()
        .pipe(
          map(movies => ({type: ActionTypes.LoadMoviesSuccess, payload: {movies: movies}})),
          catchError(() => of({type: ActionTypes.LoadMoviesError}))
        ))
    );


  constructor(private actions$: Actions) {
  }

  private loadMovies(): Observable<Movie[]> {
    return of([new Movie('Harry Potter', 1, 1996), new Movie('Titanic', 11, 1991)]);
  }
}
