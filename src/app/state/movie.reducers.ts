import {Movie} from '../model/movie.mode';
import * as Movies from './movie.actions';
import {createSelector} from '@ngrx/store';

export interface State {
  movies: Movie[];
  loading: boolean;
}

export const initialState: State = {
  movies: [],
  loading: false
};

export function reducer(state = initialState, action: Movies.ActionsUnion): State {
  switch (action.type) {
    case Movies.ActionTypes.LoadMovies: {
      return {
        ...state,
        loading: true
      };
    }

    case Movies.ActionTypes.LoadMoviesSuccess: {
      return {
        movies: action.payload.movies,
        loading: false
      };
    }

    case Movies.ActionTypes.LoadMoviesError: {
      return {
        ...state,
        loading: false
      };
    }

    default: {
      return state;
    }
  }
}
