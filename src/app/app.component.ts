import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {selectLoading, State} from './state/movie.reducers';
import {ActionTypes, LoadMovies} from './state/movie.actions';
import {Observable} from 'rxjs';
import {Movie} from './model/movie.mode';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-ngrx-demo';

  movies$: Observable<Movie[]> = this._store.pipe(select('movie', 'movies'));
  loading$: Observable<boolean> = this._store.pipe(select('movie', 'loading'));

  constructor(private _store: Store<State>) {
  }

  ngOnInit() {
  }

  loadMovies(): void {
    this._store.dispatch(new LoadMovies());
  }
}
